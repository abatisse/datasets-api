# In a production settings, we would certainly use an official
# image, such as tiangolo/uvicorn-gunicorn-fastapi-docker.
FROM python:3.10-slim

EXPOSE 8000
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100
WORKDIR /app

COPY requirements/requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .

CMD ["gunicorn",  "-c", "gunicorn.conf.py"]

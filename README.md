
# datasets-api

This project is a web API to query the [PAWS dataset](https://huggingface.co/datasets/paws) using Full-Text Search.

## API Reference

Redoc documentation is available at `/docs`.

#### Match query using Full-Text Search

```http
  GET /fts
```
```json
{
    "match": "Octob* AND Paris",
}
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `limit` | `int` | Return the best N results |



## What could be next?

- There is not much to test for now, but let us set up unit tests in CI pipeline!
- Hugging Face `datasets` library stores files using Apache Arrow file format.
  We should look deeper to integrate this format with memory-mapped files.
  Duckdb is interesting as it enables SQL queries on Arrow ipc files and Full-Text Search.
  Something to keep in mind for the future?
- `CORSMiddleware` was added because this API could be used from the datasets viewer.
  In the same spirit, we should add authentication middleware.
- Scalability is entrusted to gunicorn with the `workers` param, but we should monitor performances to scale the app in a production setting.
- Our match query is quite simple. How about making it robust to typos using edit distance?


## Contributing

Python >= 3.10 required.

Dependencies are pinned using pip-tools.
You may update the versions using

```bash
pip-compile --no-emit-index-url -U --output-file=requirements/requirements.txt requirements/requirements.in
```
```bash
pip-sync requirements/requirements.txt
```

Contributors may run the app locally by using the provided `run.py` or `docker-compose.yml` files.

```bash
python run.py
```
OR
```bash
docker-compose up --build
```

The application will then run on http://127.0.0.1:8000.

import contextlib

from fastapi import FastAPI
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware

from app.config import Settings, get_settings
from app.db import get_db_session, maybe_create_fts_index
from app.router import router


def create_app(settings: Settings | None = None) -> FastAPI:
    """Define application factory."""
    if settings is None:
        settings = get_settings()

    middlewares = []

    if settings.BACKEND_CORS_ORIGINS:
        middlewares.append(
            Middleware(
                CORSMiddleware,
                allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
                allow_credentials=True,
                allow_methods=["*"],
                allow_headers=["*"],
            )
        )

    app = FastAPI(
        title="Dataset API",
        docs_url=None,
        redoc_url="/docs",
        version="0.1.0",
        middleware=middlewares,
        description="Full text search using the PAWS EN dataset",
    )
    app.include_router(router)

    return app


get_db_wrapper = contextlib.contextmanager(get_db_session)
# Running this in a global context allows us to do
# the check only once instead of one per worker.
# This setup should come in handy if we were to load a large model,
# or share a memory-mapped file across multiple processes.
with get_db_wrapper(get_settings()) as session:
    maybe_create_fts_index(session)

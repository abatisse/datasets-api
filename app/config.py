"""Application settings loader and defaults."""
from functools import lru_cache
from pathlib import Path
from typing import List, Union

from pydantic import AnyHttpUrl, BaseSettings, validator


class Settings(BaseSettings):
    """Populate application settings.

    This class is accessible from the entire project and automatically finds
    environment variables, including from dotenv files.
    """

    DB_DATA_PATH: Path = "data/paws.sqlite"
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def _assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


@lru_cache
def get_settings() -> Settings:
    """Load settings.

    This function uses a cache to load only once the parameters.
    We avoid systematic access to disk should we use a .env file.
    """
    return Settings()

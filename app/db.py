import sqlite3

from fastapi import Depends

from app.config import Settings, get_settings
from app.models import MatchQuery, MatchResult


def get_db_session(settings: Settings = Depends(get_settings)) -> sqlite3.Connection:
    """Make a single db session per request."""
    with sqlite3.connect(settings.DB_DATA_PATH, check_same_thread=False) as connection:
        yield connection


def maybe_create_fts_index(db: sqlite3.Connection = Depends(get_db_session)):
    """Create FTS index if needed"""
    table = db.execute(
        """
        SELECT name
        FROM sqlite_master
        WHERE type = 'table'
          AND name = 'train_fts'
        """
    ).fetchone()
    if table is None:
        db.execute(
            """
            CREATE VIRTUAL TABLE train_fts USING fts5
            (
                id UNINDEXED,
                sentence1,
                sentence2,
                label UNINDEXED,
                content_rowid='id',
                tokenize='porter unicode61'
            )
            """
        )
        db.execute(
            """
        INSERT INTO train_fts
        SELECT *
        FROM train
        """
        )


def retrieve_docs_from_db(
    db: sqlite3.Connection, query: MatchQuery, limit: int
) -> MatchResult:
    results = db.execute(
        "SELECT * FROM train_fts WHERE sentence1 MATCH (?) LIMIT (?)",
        (query.match, limit),
    ).fetchall()
    return results

from pydantic import BaseModel


class MatchQuery(BaseModel):
    match: str

    class Config:
        schema_extra = {
            "example": {
                "match": "Octob* AND Paris",
            }
        }


MatchResult = list[tuple[int, str, str, int]]

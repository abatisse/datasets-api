"""Application routes.

Since this application is quite simple, we may regroup multiple
functionalities in a single file.
In a more complex setting, we could split the route into several files/router
for ease of maintenance.
"""
import sqlite3

from fastapi import APIRouter, Depends, Query

from app.db import get_db_session, retrieve_docs_from_db
from app.models import MatchQuery, MatchResult

LIMIT_MATCH = 25

router = APIRouter()


@router.get("/status")
def health_check():
    """Return HTTP 200 for monitoring.

    This route may be used as a simple application health check.
    """
    pass


@router.get("/fts", response_model=MatchResult)
def fts_match(
    query: MatchQuery,
    limit: int = Query(
        default=LIMIT_MATCH, gt=0, le=25, description="Return the N best ranked results"
    ),
    db: sqlite3.Connection = Depends(get_db_session),
):
    """Run a Full-Text Search match query on PAWS dataset.

    A match query is composed of tokens. Queries may be combined
    using boolean operators and prefix tokens.

    - `Octob* AND Paris` will match documents containing tokens begining with
    `Octob` and `Paris`.
    - `England NOT London` will match documents containing `England` but not `London`
    """
    results = retrieve_docs_from_db(db, query, limit)
    return results

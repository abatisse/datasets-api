wsgi_app = "app.app:create_app()"
bind = "0.0.0.0:8000"
workers = 2
worker_class = "uvicorn.workers.UvicornWorker"
accesslog = "-"
worker_tmp_dir = "/dev/shm"
preload_app = True

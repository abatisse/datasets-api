import uvicorn

if __name__ == "__main__":
    uvicorn.run("app.app:create_app", port=8000, reload=True, reload_dirs=["app"])

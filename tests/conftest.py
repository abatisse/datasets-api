import sqlite3

import pytest
from fastapi import FastAPI
from starlette.testclient import TestClient

from app.db import get_db_session
from app.router import router


@pytest.fixture(name="session")
def session_fixture() -> sqlite3.Connection:
    with sqlite3.connect(":memory:", check_same_thread=False) as session:
        yield session


@pytest.fixture(name="client")
def client_fixture(session: sqlite3.Connection):
    def get_session_override():
        return session

    app = FastAPI()
    app.include_router(router)
    app.dependency_overrides[get_db_session] = get_session_override

    client = TestClient(app)
    yield client
    app.dependency_overrides.clear()

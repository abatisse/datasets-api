import sqlite3
from urllib.parse import urlencode

import pytest
from starlette import status
from starlette.testclient import TestClient

from app.db import maybe_create_fts_index


def test_health_check(client: TestClient):
    # Given
    # When
    response = client.get("/status")
    # Then
    assert response.status_code == status.HTTP_200_OK


@pytest.fixture()
def fill_db(session: sqlite3.Connection):
    session.execute(
        """
        CREATE TABLE train
        (
            id        integer primary key autoincrement,
            sentence1 text,
            sentence2 text,
            label     int
        )
        """
    )
    for i in range(30):
        session.execute(
            """
            INSERT INTO train VALUES (?, ?, ?, ?)
            """,
            (i, "sentence test", "sentence test", 0),
        )
    maybe_create_fts_index(session)


def test_limit_param_ok(client: TestClient, fill_db):
    # Given
    query = dict(match="test")
    # When
    response = client.get("/fts", json=query)
    # Then
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 25


def test_limit_param_over(client: TestClient, fill_db):
    # Given
    query = dict(match="test")
    params = urlencode(dict(limit=30))
    # When
    response = client.get(f"/fts?{params}", json=query)
    # Then
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
